package ru.madela.pattern.userserver.controller;

public final class UrlPathDefinitions {
    private static final String PAGE_VARIABLE = "/{page}";
    public static final String ROOT = "/";
    public static final String USER_ROOT_URL = ROOT + "user";
    public static final String USER_GET_URL = "/get";
    public static final String GET_ALL_USERS_URL = USER_ROOT_URL + "/all" + PAGE_VARIABLE;
    public static final String GET_USER_BY_NAME_OR_SURNAME_URL = USER_ROOT_URL +
            USER_GET_URL + "/{target}" + PAGE_VARIABLE;
    public static final String GENERATE_USER_URL = USER_ROOT_URL + "/generate";

    private UrlPathDefinitions() {

    }
}
