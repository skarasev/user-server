package ru.madela.pattern.userserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.madela.pattern.userserver.dto.UserDto;
import ru.madela.pattern.userserver.exception.UserNotFoundException;
import ru.madela.pattern.userserver.service.IUserService;
import ru.madela.pattern.userserver.service.IGenerator;

import java.util.List;

import static ru.madela.pattern.userserver.controller.UrlPathDefinitions.*;

@RestController
public class UserController {
    private final IUserService userService;
    private final IGenerator userGenerator;

    @Autowired
    public UserController(IUserService userService, IGenerator userGenerator) {
        this.userService = userService;
        this.userGenerator = userGenerator;
    }

    @GetMapping(GET_ALL_USERS_URL)
    public ResponseEntity<List<UserDto>> getAllUsers(@PathVariable(name = "page") Integer page)
            throws UserNotFoundException {
        return ResponseEntity.ok(userService.getAllUsers(page));
    }

    @GetMapping(GET_USER_BY_NAME_OR_SURNAME_URL)
    public ResponseEntity<List<UserDto>> getUsersByNameOrSurname(@PathVariable(name = "target") String target,
                                                                 @PathVariable(name = "page") Integer page)
            throws UserNotFoundException {
        return ResponseEntity.ok(userService.getUsersByNameOrSurname(target, page));
    }

    @GetMapping(GENERATE_USER_URL)
    public ResponseEntity<String> generateUsers() {
        userGenerator.generate();
        return ResponseEntity.ok("Generating was started");
    }
}
