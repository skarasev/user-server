package ru.madela.pattern.userserver.service;

public interface IGenerator {
    void generate();
}
