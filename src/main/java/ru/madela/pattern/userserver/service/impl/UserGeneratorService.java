package ru.madela.pattern.userserver.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.madela.pattern.userserver.dto.UserDto;
import ru.madela.pattern.userserver.store.UserMemoryStore;
import ru.madela.pattern.userserver.service.IGenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

@Component
@Slf4j
public class UserGeneratorService implements IGenerator {
    private final UserMemoryStore userMemoryStore;
    private final ObjectMapper objectMapper;

    @Autowired
    public UserGeneratorService(UserMemoryStore userMemoryStore, ObjectMapper objectMapper) {
        this.userMemoryStore = userMemoryStore;
        this.objectMapper = objectMapper;
    }

    @Override
    public void generate() {
        Supplier<List<String>> nameListSupplier = getSupplier("names.txt");
        Supplier<List<String>> surnameListSupplier = getSupplier("surnames.txt");
        getCompletableFuture(nameListSupplier)
                .thenCombine(getCompletableFuture(surnameListSupplier), this::generateNewUsersRandom)
                .thenApplyAsync(users -> {
                    userMemoryStore.setUserList(users);
                    return users;
                })
                .thenAcceptAsync(this::recordToJson);
    }

    private void recordToJson(List<UserDto> users) {
        URL url = getClass().getClassLoader().getResource("users.json");
        if (url == null) {
            return;
        }
        try {
            objectMapper.writeValue(new BufferedWriter(new FileWriter(url.getFile())), users);
        } catch (IOException e) {
            log.error("Users did not wrote to the json file");
        }
    }

    /**
     * @deprecated use generateNewUsersRandom
     */
    @Deprecated
    private List<UserDto> generateNewUsers(List<String> names, List<String> surnames) {
        List<UserDto> users = new ArrayList<>();
        names.forEach(name -> surnames.forEach(
                surname -> names.forEach(
                        secondName -> {
                            UserDto userDto = new UserDto(name, surname, secondName);
                            users.add(userDto);
                        })
                )
        );
        return users;
    }

    private List<UserDto> generateNewUsersRandom(List<String> names, List<String> surnames) {
        List<UserDto> users = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 200_000; i++) {
            String name = names.get(random.nextInt(names.size()));
            String surname = surnames.get(random.nextInt(names.size()));
            String secondName = names.get(random.nextInt(names.size()));
            users.add(new UserDto(name, surname, secondName));
        }
        return users;
    }

    private Supplier<List<String>> getSupplier(String filename) {
        return () -> {
            URL fileUrl = UserGeneratorService.class.getClassLoader().getResource(filename);
            if (fileUrl == null) {
                return new ArrayList<>();
            }
            List<String> lines;
            try {
                lines = Files.readAllLines(Path.of(fileUrl.toURI()));
            } catch (IOException | URISyntaxException e) {
                return new ArrayList<>();
            }
            return lines;
        };
    }

    private CompletableFuture<List<String>> getCompletableFuture(Supplier<List<String>> supplier) {
        return CompletableFuture.supplyAsync(supplier);
    }
}
