package ru.madela.pattern.userserver.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.madela.pattern.userserver.exception.UserNotFoundException;
import ru.madela.pattern.userserver.dto.UserDto;
import ru.madela.pattern.userserver.service.IUserService;
import ru.madela.pattern.userserver.store.UserMemoryStore;
import ru.madela.pattern.userserver.util.KnuthMorrisPrattAlgorithmSearchUtil;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserService implements IUserService {
    private static final int LOAD_FACTOR = 50;
    private final UserMemoryStore userMemoryStore;

    @Autowired
    public UserService(UserMemoryStore userMemoryStore) {
        this.userMemoryStore = userMemoryStore;
    }

    @Override
    public List<UserDto> getAllUsers(int page) throws UserNotFoundException {
        if (page < 1) {
            throw new UserNotFoundException("User list not found");
        }
        int skipFactor = page == 1 ? 0 : (page - 1) * LOAD_FACTOR;
        List<UserDto> users = userMemoryStore.getUserList()
                .parallelStream()
                .skip(skipFactor)
                .limit(LOAD_FACTOR)
                .collect(Collectors.toList());
        logUserListSize(users);
        if (users.isEmpty()) {
            throw new UserNotFoundException("User list is empty");
        }
        return users;
    }

    private void logUserListSize(List<UserDto> users) {
        log.info("user list size is {}", users.size());
    }

    @Override
    public List<UserDto> getUsersByNameOrSurname(String target, int page) throws UserNotFoundException {
        if (page < 1) {
            throw new UserNotFoundException("User list not found");
        }
        int skipFactor = page == 1 ? 0 : (page - 1) * LOAD_FACTOR;
        List<UserDto> founded = userMemoryStore.getUserList()
                .parallelStream()
                .filter(user -> KnuthMorrisPrattAlgorithmSearchUtil.isSearch(user.getFullName(), target))
                .skip(skipFactor)
                .limit(LOAD_FACTOR)
                .collect(Collectors.toList());
        logUserListSize(founded);
        if (founded.isEmpty()) {
            throw new UserNotFoundException("User not found");
        }
        return founded;
    }

    @Override
    public boolean addUser(UserDto user) {
        return userMemoryStore.getUserList().add(user);
    }
}
