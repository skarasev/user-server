package ru.madela.pattern.userserver.store;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.madela.pattern.userserver.dto.UserDto;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class UserMemoryStore {

    @Getter
    @Setter
    private List<UserDto> userList;

    private final ObjectMapper objectMapper;

    @Value(value = "${users.memory.json.file}")
    private String file;

    @Autowired
    public UserMemoryStore(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        userList = new ArrayList<>();
    }

    @PostConstruct
    private void postConstruct() {
        try {
            URL url = getClass().getClassLoader().getResource(file);
            if (url == null) {
                return;
            }
            userList = objectMapper.readValue(
                    new BufferedReader(new FileReader(url.getFile())),
                    new TypeReference<>() {
                    }
            );
        } catch (IOException e) {
            log.info("Can't to read a file: {}", e.getMessage());
        }
    }

    public boolean addUser(UserDto userDto) {
        return userList.add(userDto);
    }
}
