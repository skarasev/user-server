package ru.madela.pattern.userserver;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.madela.pattern.userserver.dto.UserDto;
import ru.madela.pattern.userserver.exception.UserNotFoundException;
import ru.madela.pattern.userserver.service.IUserService;
import ru.madela.pattern.userserver.store.UserMemoryStore;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    @MockBean
    private UserMemoryStore store;

    @Test
    public void getAllUsersTest() throws UserNotFoundException {
        Mockito.when(store.getUserList())
                .thenReturn(List.of(new UserDto(), new UserDto(), new UserDto()));
        List<UserDto> userDtoList = userService.getAllUsers(1);
        Assertions.assertFalse(userDtoList.isEmpty());
        Assertions.assertEquals(3, userDtoList.size());
    }

    @Test
    public void searchUsersByNameOrSurnameTest() throws UserNotFoundException {
        Mockito.when(store.getUserList())
                .thenReturn(List.of(new UserDto("Richard", "McCall", "Braun")));
        List<UserDto> userDtoList = userService.getUsersByNameOrSurname("mccall", 1);
        Assertions.assertFalse(userDtoList.isEmpty());
        Assertions.assertEquals(1, userDtoList.size());
    }

    @Test(expected = UserNotFoundException.class)
    public void getAllUsersPageUserNotFoundExceptionTest() throws UserNotFoundException {
        Mockito.when(store.getUserList())
                .thenReturn(List.of(new UserDto(), new UserDto(), new UserDto()));
        List<UserDto> userDtoList = userService.getAllUsers(2);
        Assertions.assertTrue(userDtoList.isEmpty());
    }

    @Test(expected = UserNotFoundException.class)
    public void searchUsersByNameOrSurnamePageUserNotFoundExceptionTest() throws UserNotFoundException {
        Mockito.when(store.getUserList())
                .thenReturn(List.of(new UserDto("Richard", "McCall", "Braun")));
        List<UserDto> userDtoList = userService.getUsersByNameOrSurname("mccall", 10);
        Assertions.assertTrue(userDtoList.isEmpty());
    }

    @Test(expected = UserNotFoundException.class)
    public void getAllUsersUserNotFoundExceptionTest() throws UserNotFoundException {
        Mockito.when(store.getUserList())
                .thenReturn(new ArrayList<>());
        List<UserDto> userDtoList = userService.getAllUsers(1);
        Assertions.assertTrue(userDtoList.isEmpty());
    }

    @Test(expected = UserNotFoundException.class)
    public void searchUsersByNameOrSurnameUserNotFoundExceptionTest() throws UserNotFoundException {
        Mockito.when(store.getUserList())
                .thenReturn(new ArrayList<>());
        List<UserDto> userDtoList = userService.getUsersByNameOrSurname("mccall", 1);
        Assertions.assertTrue(userDtoList.isEmpty());
    }
}
